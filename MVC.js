/**
 * @class Base
 */
class Base{
	
	constructor(){
		this._events = {};
	}
	
	on( event , handler , context = null ){
		
		if( !this._events[ event ] ) this._events[ event ] = [];
		
		this._events[ event ].push( {
			h : handler ,
			c : context
		} );
		
	}
	
	off( event , handler , context = null ){
		
		if( !this._events[ event ] ) return;
		
		this._events[ event ] = this._events[ event ].filter( ( e ) => { return !( e.h === handler && e.c === context ) } );
		
	}
	
	once( event , handler , context ){
		
		let fn = ( ( h , c ) => {
			return ( ...a ) => {
				h.apply && h.apply( c , a );
				this.off( event , fn );
			}
		} )( handler , context );
		
		this.on( event , fn );

	}
	
	fire( event , ...a ){
		
		if( !this._events[ event ] ) return;
		
		this._events[ event ].forEach( ( e ) => {
			e.h.apply && e.h.apply( e.c , a );
		} );
		
	}
	
}

class Request{
	
	constructor( config = {} ){
		
		this.url = config.url || "/";
		this.method = config.method || "GET";
		this.async = config.async || true;
		this.data = config.data;
		
		this.xhr = new XMLHttpRequest();
		this.xhr.open( this.method , this.url , this.async );
		
		this.xhr.onreadystatechange = () => {
			if( this.xhr.readyState == 4 ){
				
				if( this.xhr.status == 200 ){
					
					config.success && config.success( this.xhr.response );
					
				}
				
			}
			
		};
		
		this.xhr.send( this.data );
		
	}
	
	get url(){
		return this._url;
	}
	set url( value ){
		this._url = value;
	}
	
	get method(){
		return this._method;
	}
	set method( value ){
		this._method = value.toUpperCase();
	}
	
	get async(){
		return this._async || true;
	}
	set async( value ){
		this._async = value;
	}
	
}

function $( selector , parent ){
	
	return new DOMOperator( selector , parent );
	
}

class DOMOperator {
	
	constructor( selector , parent ){
		
		this._elements = document.querySelectorAll( selector );
		
	}
	
	get length(){
		return this._elements.length;
	}
	
	each(){}
	
	empty(){}
	
	remove(){}
	
	append( nodes ){
		
		if( typeof nodes === "string" ){
			
			let p = document.createElement( "span" );
			p.innerHTML = nodes;
			nodes = p.childNodes;
			
		} else {
			
			nodes = Array.isArray( nodes ) ? nodes : [ nodes ];
			
		}
		
		nodes.forEach( ( node ) => {
			this._elements.item( 0 ).appendChild( node );
		} );
		
		return this;
		
	}
	
	addClass( name ){}
	
	removeClass( name ){}
	
	toggleClass(){}
	
}

/**
 * @class Model
 * @extends Base
 */
class Model extends Base{
	
	constructor( config = {} ){
		
		super( config );
		
		this._struct = config.model || {};
		this._reader = config.reader || ( ( d ) => { return d } );
		this._writer = config.writer || ( ( d ) => { return d } );
		this._exportFields = new Set();
		this._MVCModel = true;
		this._watchers = {};
		
		if( config.exports ){
			config.exports = Array.isArray( config.exports ) ? config.exports : [ config.exports ];
			config.exports.forEach( ( e ) => {
				this._exportFields.add( e );
			} );
		}
		
		this._proxy = new Proxy( this , {
			
			get : ( obj , prop ) => {
				
				let value = obj[ prop ];
					
				if( prop in obj._struct && typeof obj._struct[ prop ].get === "function" ){
					value = obj._struct[ prop ].get( obj , prop , value );
				}
				
				if( typeof value === "function" ) value = value.bind( obj );
				
				if( value !== undefined ) return value;
				
				if( prop in obj._struct ){
					value = obj._struct[ prop ].default;
				}
				
				return value;
				
			} ,
			set : ( obj , prop , value ) => {
				
				if( prop in obj._struct && typeof obj._struct[ prop ].set === "function" ){
					value = obj._struct[ prop ].set( obj , prop , value );
				}
				
				let old = obj[ prop ];
				
				obj[ prop ] = value;
				
				obj.fire( "propertyChanged" , {
					prop : prop ,
					new : value ,
					old : old
				} );
				
				old = null;
				
			}
			
		} );
		
		this.on( "propertyChanged" , this.runWatchers.bind( this ) );
		
		return this._proxy;
		
	}
	
	apply( data ){
		
		for( let i in data ){
			
			this[ i ] = data[ i ];
			this._exportFields.add( i );
			
		}
		
	}
	
	load(){}
	
	save(){}
	
	exportData(){
		
		let data = {};
		
		this._exportFields.forEach( ( f ) => {
			data[ f ] = this._proxy[ f ];
		} );
		
		return this._writer( data );
		
	}
	
	runWatchers( changedObj ){
		
		if( this._watchers[ changedObj.prop ] ){
			
			this._watchers[ changedObj.prop ].forEach( w => {
				w.h.apply && w.h.call( w.c , this._proxy , changedObj );
			} );
			
		}
		
	}
	
	watch( prop , handler , context = null ){
		
		if( !this._watchers[ prop ] ) this._watchers[ prop ] = [];
		
		this._watchers[ prop ].push( {
			h : handler ,
			c : context
		} );
		
	}
	
	unwatch( prop , handler , context = null ){
		
		if( !this._watchers[ prop ] ) return;
		
		this._watchers[ prop ] = this._watchers[ prop ].filter( ( w ) => { return !( w.h === handler && w.c === context ) } );
		
	}
	
}

/**
 * @class Collection
 * @extends Base
 */
class Collection extends Base {
	
	constructor( config = {} ){
		
		super( config );
		this._modelType = config.model;
		this._stack = [];
		this._server = config.server;
		this._toInsert = [];
		this._toRemove = [];
		this._toUpdate = [];
		this._modelIdentifier = config.identifier || "id";
		
	}
	
	/**
	 * Requests a list of models from back-end.
	 * Default method GET
	 */
	load(){
		
		if( !this._server ) throw "No back-end set for this operation";
		
	}
	
	/**
	 * Sends an array of elements with no model identifier set to back-end.
	 * For example, if the identifier of a model is "id", all the element with (bool)id == false will be sent
	 * Default method PUT
	 */
	insert(){
		
		if( !this._server ) throw "No back-end set for this operation";
		
		let list = this._stack.filter( ( i ) => { return !i[ this._modelIdentifier ] } );
		
		console.log( `Lists to update ${list}` );
		
	}
	
	/**
	 * Sends an array of elements with modified data only to back-end.
	 * Default method POST
	 */
	update(){
		
		if( !this._server ) throw "No back-end set for this operation";
	
	}
	
	/**
	 * Sends an array of model identifiers that were removed to back-end.
	 * Default method DELETE
	 */
	erase(){
		
		if( !this._server ) throw "No back-end set for this operation";
	
	}
	
	/**
	 * Runs through insert, update and erase.
	 */
	saveAll(){
		
		this.insert();
		this.update();
		this.erase();
		
	}
	
	/**
	 * Adds data to the collection
	 * If model type is defined, a new Model will be created and then added.
	 * @param {(Model|Object)} data
	 */
	add( data ){
		
		data = Array.isArray( data ) ? data : [ data ];
		
		data.forEach( ( elem ) => {
			
			let data = elem;
			
			if( !elem._MVCModel && this._modelType ){
				
				data = new Model( this._modelType );
				data.apply( elem );
			}
			
			this._stack.push( data );
			
		} );
		
	}
	
	/**
	 * Removes data from the collection.
	 * If data is of type {@link Model}, will match the whole object by "===".
	 * Else data considered a query object and will match properties by "==".
	 * @param data
	 */
	remove( data ){
	}
	
}

class View extends Base{
	
	static get GloabalTemplates(){
		
		if( !View._gTemplates ) View._gTemplates = {};
		
		return View._gTemplates;
		
	}
	
	constructor( config ){
		
		super( config );
		
		this.on( "template:ready" , this._checkReady.bind( this ) );
		this.on( "css:ready" , this._checkReady.bind( this ) );
		
		if( config.template ){
			this._getTemplate( config.template );
		} else {
			this._template = true;
		}
		
		if( config.css ){
			this._getCss( config.css );
		} else {
			this._css = true;
		}
		
		this._checkReady();
		
		this._parent = config.parent || "body";
		
	}
	
	_checkReady(){
		
		this.ready = this._css && this._template;
		
	}
	
	get ready(){
		return this._ready;
	}
	set ready( value ){
		
		this._ready = value;
		if( value ){
			this.fire( "ready" );
		}
		
	}
	
	_getTemplate( template ){
		
		let tpl = View.GloabalTemplates[ template ];
		
		if( tpl ){
			
			this._template = tpl === true ? template : tpl;
			
			this.fire( "template:ready" );
			return;
			
		}
		
		if( template.toLowerCase().match( /\.(tpl|html|txt)$/ ) ){
			
			new Request( {
				url : template ,
				method : "GET" ,
				success : ( tpl ) => {
					View.GloabalTemplates[ template ] = tpl;
					this._template = tpl;
					
					this.fire( "template:ready" );
				}
			} );
			
		} else { // consider as inline template
			View.GloabalTemplates[ template ] = true;
			this._template = template;
			
			this.fire( "template:ready" );
		}
		
	}
	
	_getCss( cssPath ){
		
		let link = $( `[src="${cssPath}"]` );
		
		if( link.length ){
			
			this.fire( "css:ready" );
			
		} else {
			
			link = document.createElement( "link" );
			link.href = cssPath;
			link.rel = "stylesheet";
			link.type = "text/css";
			
			$( "head" )
				.append( link );
			
			link.onload = () => {
				this._css = true;
				this.fire( "css:ready" );
			};
			
		}
		
	}
	
	render(){
		
		if( !this.ready ){
			this.once( "ready" , this.render.bind( this ) );
			return;
		}
		
		this.$elem = $( this._parent ).append( this._template );
		
	}
	
}
	
